|	TYPE    |	RESOURCE			|	DEFAULT VALUE			|	[ALTERNATIVE RESOURCE]	|
|:---------:|:---------------------:|:-------------------------:|:-------------------------:|
|	SA		|	font				|	monospace:size=9		|							|
|	S		|	background			|	#222222					|							|
|	S		|	foreground			|	#cccccc					|							|
|	S		|	selbackground		|	#555555					|	foreground				|
|	S		|	selforeground		|	#ffffff					|	background				|
|	S		|	urgbackground		|	#111111					|							|
|	S		|	urgforeground		|	#cc0000					|							|
